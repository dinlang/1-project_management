---
title: Copier des réunions DinLang sur votre Google Calendar
abstract: "Copy DinLang meetings to your own calendar in Google Calendar"
author:
- Coralie VINCENT -- CNRS, SFL - UMR7023 -- Projet DinLang
<!--  institute: CNRS, SFL - UMR7023 -- Projet DinLang-->
date: 2021-04-14
---
Sur <https://calendar.google.com/> :

1. Par défaut, désactiver l'agenda DinLang

<kbd>![desactiver](2021-04-14-Google_Calendar-Copy_DinLang_meetings_1.png)</kbd>

2. Quand Aliyah signale les réunions à venir sur la [liste de diffusion DinLang](https://listes.services.cnrs.fr/wws/info/dinlang), réactiver l'agenda DinLang si une réunion vous concerne

<kbd>![reactiver](2021-04-14-Google_Calendar-Copy_DinLang_meetings_2.png)</kbd>

3. Faire un clic gauche sur l'événement DinLang à ajouter à votre propre agenda

4. Cliquer gauche sur les trois points en haut à droite de l'événement (« Options »)

<kbd>![options](2021-04-14-Google_Calendar-Copy_DinLang_meetings_3.png)</kbd>

5. Cliquer gauche sur « Copier dans Votre Agenda »

6. Faire d'éventuelles modifications (intitulé par exemple) et cliquer sur « Enregistrer »

<kbd>![enregistrer](2021-04-14-Google_Calendar-Copy_DinLang_meetings_4.png)</kbd>

7. Re-désactiver l'agenda DinLang (cf 1.)
